$("document").ready(function () {
    // Img Upload
    var container = $('.container'), inputFile = $('#file'), img, btn, txt = 'Browse', txtAfter = 'Browse another pic';

    if (!container.find('#upload').length) {
        container.find('.input').append('<input type="button" value="' + txt + '" id="upload">');
        btn = $('#upload');
        container.prepend('<img src="" class="hidden" alt="Uploaded file" id="uploadImg" width="100">');
        img = $('#uploadImg');
    }

    btn.on('click', function () {
        img.animate({ opacity: 0 }, 300);
        inputFile.click();
    });

    inputFile.on('change', function (e) {
        container.find('label').html(inputFile.val());

        var i = 0;
        for (i; i < e.originalEvent.srcElement.files.length; i++) {
            var file = e.originalEvent.srcElement.files[i],
                reader = new FileReader();

            reader.onloadend = function () {
                img.attr('src', reader.result).animate({ opacity: 1 }, 700);
            }
            reader.readAsDataURL(file);
            img.removeClass('hidden');
        }

        btn.val(txtAfter);
    });

    $('#input-file').on('change', function () {
        //get the file name
        var fileName = $(this).val();
        //replace the "Choose a file" label
        $(this).next('.custom-file-label').html(fileName);
    })
    // CUSTOM TABS
    $(".tab-slider--body").hide();
    $(".tab-slider--body:first").show();
    // DATE TIME PICKER
    // var picker = new MaterialDatetimePicker({})
    //     .on('submit', function (d) {
    //         output.innerText = d;
    //     });
    // var el = document.querySelector('.c-datepicker-btn');
    // el.addEventListener('click', function () {
    //     picker.open();
    // }, false);


    $(".tab-slider--nav li").click(function () {
        $(".tab-slider--body").hide();
        var activeTab = $(this).attr("rel");
        $("#" + activeTab).fadeIn();
        if ($(this).attr("rel") == "tab2") {
            $('.tab-slider--tabs').addClass('slide');
        } else {
            $('.tab-slider--tabs').removeClass('slide');
        }
        $(".tab-slider--nav li").removeClass("active");
        $(this).addClass("active");
    });
    if ($('.datetimePicker').length > 0) {
        $('.datetimePicker').bootstrapMaterialDatePicker({

            date: true,

            // enable time picker

            time: true,

            // custom date format

            format: 'YYYY-MM-DD - HH:mm',
            //min / max date

            minDate: null,

            maxDate: null,


            // current date

            currentDate: null,

            // Localization

            lang: 'en',

            // week starts at
            weekStart: 0,
            // short time format
            shortTime: true,
            // text for cancel button

            'cancelText': 'Cancel',
            // text for ok button

            'okText': 'OK'

        });
    }

    $('.page-item').click(function() {
        $(this).addClass('active').siblings().removeClass('active');
    });

    if($('select').length > 0 ){
        $('select').selecty();
    }

    $('input[name="files"]').fileuploader({
        startImageRenderer: true,
        synchronImages: true,
        theme: 'default',
        changeInput: true,
        thumbnails: {
            // thumbnails list HTML {String, Function}
            // example: '<ul></ul>'
            // example: function(options) { return '<ul></ul>'; }
            box: '<div class="fileuploader-items">' +
                      '<ul class="fileuploader-items-list"></ul>' +
                  '</div>',
                  
            // append thumbnails list to selector {null, String, jQuery Object}
            // example: 'body'
            boxAppendTo: null,
            
            // thumbnails for the choosen files {String, Function}
            // example: '<li>${name}</li>'
            // example: function(item) { return '<li>' + item.name + '</li>'; }
            item: '<li class="fileuploader-item file-has-popup">' +
                       '<div class="columns">' +
                           '<div class="column-thumbnail">${image}<span class="fileuploader-action-popup"></span></div>' +
                           '<div class="column-title">' +
                               '<div title="${name}">${name}</div>' +
                               '<span>${size2}</span>' +
                           '</div>' +
                           '<div class="column-actions">' +
                               '<a class="fileuploader-action fileuploader-action-remove" title="${captions.remove}"><i></i></a>' +
                           '</div>' +
                       '</div>' +
                       '<div class="progress-bar2">${progressBar}<span></span></div>' +
                  '</li>',
                  
            // thumbnails for the preloaded files {String, Function}
            // example: '<li>${name}</li>'
            // example: function(item) { return '<li>' + item.name + '</li>'; }
            item2: '<li class="fileuploader-item file-has-popup">' +
                        '<div class="columns">' +
                            '<div class="column-thumbnail">${image}<span class="fileuploader-action-popup"></span></div>' +
                            '<div class="column-title">' +
                                '<a href="${file}" target="_blank">' +
                                    '<div title="${name}">${name}</div>' +
                                    '<span>${size2}</span>' +
                                '</a>' +
                            '</div>' +
                            '<div class="column-actions">' +
                                '<a href="${file}" class="fileuploader-action fileuploader-action-download" title="${captions.download}" download><i></i></a>' +
                                '<a class="fileuploader-action fileuploader-action-remove" title="${captions.remove}"><i></i></a>' +
                            '</div>' +
                        '</div>' +
                    '</li>',
                    
            // thumbnails selectors
            _selectors: {
                list: '.fileuploader-items-list',
                item: '.fileuploader-item',
                start: '.fileuploader-action-start',
                retry: '.fileuploader-action-retry',
                remove: '.fileuploader-action-remove',
                sorter: '.fileuploader-action-sort',
                rotate: '.fileuploader-action-rotate',
                popup: '.fileuploader-popup-preview',
                popup_open: '.fileuploader-action-popup'
            },
            
            // insert the thumbnail's item at the begining of the list? {Boolean}
            itemPrepend: false,
            
            // show a confirmation dialog by removing a file? {Boolean}
            // it will not be shown in upload mode by canceling an upload
            // you can call your own dialog box using dialogs option
            removeConfirmation: true,
            
            // render the image thumbnail? {Boolean}
            // if false, it will generate an icon(you can also hide it with css)
            // if false, you can use the API method item.renderThumbnail() to render it (check thumbnails example)
            startImageRenderer: true,
            
            // render the images synchron {Boolean}
            // used to improve the browser speed
            synchronImages: true,
            
            // read image using URL createObjectURL method {Boolean}
            // if false, it will use readAsDataURL
            useObjectUrl: false,
            
            // render the image in a canvas element {Boolean, Object}
            // if true, it will generate an image with the css sizes from the parent element of ${image}
            // you can also set the width and the height in the object {width: 96, height: 96}
            canvasImage: true,
            
            // render thumbnail for video files? {Boolean}
            videoThumbnail: false,
            
            // fix exif orientation {Boolean}
            exif: true,
            
            // enable pdf viewer {Boolean, Object}
            // example: true // browser's native
            // example: viewer: { viewer: 'assets/pdf.js/web/viewer.html?file=', urlPrefix: window.location.pathname } // pdf.js
            pdf: true,
            
            // Callback fired before adding the list element
            beforeShow: null,
            
            // Callback fired after adding the item element
            onItemShow: null,
            
            // Callback fired after removing the item element
            // by default we will animate the removing action
            onItemRemove: function(html) {
                html.children().animate({'opacity': 0}, 200, function() {
                    setTimeout(function() {
                        html.slideUp(200, function() {
                            html.remove();
                        });
                    }, 100);
                });
            },
            
            // Callback fired after the item image was loaded or a image file is invalid
            // default - null
            onImageLoaded: function(item, listEl, parentEl, newInputEl, inputEl) {
                // invalid image?
                if (item.image.hasClass('fileuploader-no-thumbnail')) {
                    // callback goes here
                }
                
                // check image size and ratio?
                if (item.reader.node && item.reader.width > 1920 && item.reader.height > 1080 && item.reader.ratio != '16:9') {
                    // callback goes here
                }
            }
            
        }
    });

    
});

function openNav() {
    document.getElementById("mySidepanel").style.width = "250px";
    // $('body').addClass('overlayBackground');
}

function closeNav() {
    document.getElementById("mySidepanel").style.width = "0";
    // $('body').removeClass('overlayBackground');
}


