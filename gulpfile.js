var gulp = require('gulp');
var sass = require('gulp-sass');

gulp.task('scss', function () {
    return gulp.src('src/scss/**/*.scss')
        .pipe(sass().on('error', sass.logError))
        .pipe(gulp.dest('./dist/css/'));
});

// gulp.task('default', function () {
//     gulp.watch('src/scss/**/*.scss', ['scss']);
// });

// SOLUTION FOR GULP 4.0
gulp.task('default', function () {
    return gulp.watch('src/scss/**/*.scss', gulp.series('scss'));
});